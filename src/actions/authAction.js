import axios from "axios";
import setAuthToken from "../utils/setAuthToken";
import jwt_decode from "jwt-decode";
import * as actionTypes from "./actionTypes"; 

var url = "http://localhost:3005" || "mongodb+srv://meilita123456:binar123456@cluster0-jy3s6.mongodb.net/test?retryWrites=true&w=majority";

//Register User
export const registerUser = (userData, history) => dispatch => {
    console.log(userData);
    axios.post("/user", userData)
        .then(res => history.push("/login"))
        .catch(err => 
            dispatch({
                type: actionTypes.GET_ERRORS,
                payload: err.response.data
            }))
}

//Login - get user token
export const loginUser = userData => dispatch => {
    axios.post("/user/login", userData)
    .then( res => {
        const { token } = res.data;
        localStorage.setItem("jwtToken", token)
        //set token to auth header
        setAuthToken(token);
        const decoded = jwt_decode(token);
        //set current user
        dispatch(setCurrentUser(decoded));
    })
    .catch(err => dispatch({
        type: actionTypes.GET_ERRORS,
        payload: err.response.data
    }));
}

//Set logged in user
export const setCurrentUser = decoded => {
    return {
        type: actionTypes.SET_CURRENT_USER,
        payload: decoded
    }
}

//User loading
export const setUserLoding = () => {
    return {
        type: actionTypes.USER_LOADING
    }
}

//log out User 
export const logoutUser = () => dispatch => {
    localStorage.removeItem("jwtToken")
    setAuthToken(false);
    dispatch(setCurrentUser({}));
    
};


//Set token to localstorage




